\raggedbottom
\input texinfo @c -*- texinfo -*-
@comment %**start of header
@setfilename codigo
@settitle Memoria y documentacion - Practica 2
@comment @paragraphindent 0
@setchapternewpage odd
@comment @footnotestyle separate
@iftex
@comment @smallbook
@afourpaper
@tolerance 10000
@hbadness 10000
@end iftex
@macro hfill
@tex
@hfill
@end tex
@end macro
@comment %**end of header

@iftex

@titlepage
@title Memoria y documentacion - Practica 2
@comment @font@authorrm=cmbx10 scaled @magstep2
@author Jorge Lopez Rosende
@comment Copyright page
@page
@vskip 0pt plus 1filll

@end titlepage

@pageno 1
@contents
@end iftex
@ifinfo


@node Top, codigo, (dir), (dir)
@top Memoria y documentacion - Practica 2
@end ifinfo
@menu
* codigo::
@end menu
@include codigointro.texic
@include codigorefs.texic
@bye
