\unnumbchapentry{codigo}{1}
\unnumbsecentry{Estructuras basicas}{1}
\unnumbsecentry{Funciones auxiliares}{1}
\unnumbsecentry{Funciones principales}{1}
\unnumbsecentry{Tests automaticos}{1}
\unnumbsubsecentry{Lanzar los test automaticamente}{1}
\unnumbsecentry{Usage and interface}{2}
\unnumbsecentry{Documentation on exports}{2}
\unnumbsubsubsecentry{alumno\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}prode/4 (pred)}{2}
\unnumbsubsubsecentry{eliminar\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}comodines/3 (pred)}{2}
\unnumbsubsubsecentry{ejecutar\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}instruccion/3 (pred)}{2}
\unnumbsubsubsecentry{generador\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}de\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}codigo/3 (pred)}{2}
\unnumbsubsubsecentry{generador\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}de\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}codigo\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}/4 (pred)}{3}
\unnumbsubsubsecentry{replace/3 (pred)}{3}
\unnumbsubsubsecentry{check\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}regs/3 (pred)}{3}
\unnumbsubsubsecentry{move/4 (pred)}{4}
\unnumbsubsubsecentry{move\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}/5 (pred)}{4}
\unnumbsubsubsecentry{swap/4 (pred)}{4}
\unnumbsubsubsecentry{filter\unhbox \voidb@x \kern .06em \vbox {\hrule width.3em height.1ex}nodes/3 (pred)}{4}
\unnumbsubsubsecentry{members/2 (pred)}{5}
\unnumbsecentry{Documentation on imports}{5}
\unnumbchapentry{References}{7}
