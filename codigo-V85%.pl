:- module(_, _, [assertions]).
:- use_module(library(aggregates)).
:- use_module(library(lists)).

alumno_prode('Lopez', 'Rosende', 'Jorge', 'A180499').

:- doc(title, "Memoria y documentacion - Practica 2").

:- doc(author, "Jorge Lopez Rosende").

replace(_, [], []).
replace(O, [O|T], [_|T2]) :- replace(O, T, T2).
replace(O, [H|T], [H|T2]) :- H \= O, replace(O, T, T2).

% removeAll(_, [], []).
% removeAll(X, [X|T], L) :- removeAll(X, T, L), !.
% removeAll(X, [H|T], [H|L]) :- removeAll(X, T, L ).

check_regs(R, N, L) :-
    functor(R, regs, N),
    N > 1,
    R =.. T,
    arg(2,T,L).

check_move(I, P) :-
    nonvar(I),
    functor(I, move, N),
    N == 1,
    arg(1, I, P),
    integer(P).

check_move(I, P) :-
    var(I),
    functor(I, move, 1),
    I =.. T,
    integer(P),
    arg(2, T, [P]).

check_swap(I, Arg1, Arg2) :-
    nonvar(I),
    functor(I, swap, N),
    N == 2,
    arg(1, I, Arg1),
    arg(2, I, Arg2),
    integer(Arg1),
    integer(Arg2),
    Arg1 < Arg2.

check_swap(I, Arg1, Arg2) :-
    var(I),
    functor(I, swap, 2),
    I =.. T,
    integer(Arg1),
    integer(Arg2),
    Arg1 < Arg2,
    arg(2, T, [Arg1, Arg2]).

eliminar_comodines(R, Rs, L) :-
    nonvar(R),
    check_regs(R, N, L2),
    replace(*, L2, L3),
    delete(L2, *, L),
    check_regs(Rs, N, L3).

eliminar_comodines(R, Rs, L) :-
    var(R),
    check_regs(Rs, N, L3),
    replace(*, L4, L3),
    members(L, L4),
    check_regs(R, N, L4).


% (In, Out, Out)
% ejecutar_instruccion(Ri, Ins, Rf) :-
%     nonvar(Ri),
%     eliminar_comodines(Ri, R1, _),
%     ejecutar_instruccion_(R1, Ins, Rf),
%     eliminar_comodines(Rf, _, _).

ejecutar_instruccion(Ri, Ins, Rf) :-
    nonvar(Ri),
    ejecutar_instruccion_(Ri, Ins, Rf).

% (In, In, Out) swap 
ejecutar_instruccion_(Ri, Ins, Rf) :-
    nonvar(Ins),
    check_regs(Ri, N, L),
    check_swap(Ins, P1, P2),
    I is P1 - 1,
    J is P2 - 1,
    I >= 0,
    J < N,
    swap(L, I, J, L2),
    check_regs(Rf, N, L2).

% (In, Out, Out) swap
ejecutar_instruccion_(Ri, Ins, Rf) :-
    var(Ins),
    check_regs(Ri, N, Li),
    swap(Li, I, J, Lf),
    check_regs(Rf, N, Lf),
    In is I + 1,
    Jn is J + 1,
    check_swap(Ins, In, Jn),
    ejecutar_instruccion(Ri, Ins, Rf).

% (In, In, Out) move 
ejecutar_instruccion_(Ri, Ins, Rf) :-
    nonvar(Ins),
    check_regs(Ri, N, L),
    check_move(Ins, P),
    P > 0,
    P < N + 1,
    I is P - 1,
    getAt(L, E, I),
    X is P mod N,
    move(L, X, E, Lf),
    check_regs(Rf, N, Lf).

% (In, Out, In) move 
ejecutar_instruccion_(Ri, Ins, Rf) :-
    var(Ins),
    check_regs(Ri, N, Li),
    check_regs(Rf, N, Lf),
    move(Li, P, _, Lf),
    P == 0,
    check_move(Ins,N),
    ejecutar_instruccion(Ri, Ins, Rf).

% (In, Out, In) move 
ejecutar_instruccion_(Ri, Ins, Rf) :-
    var(Ins),
    check_regs(Ri, N, Li),
    check_regs(Rf, N, Lf),
    move(Li, P, _, Lf),
    check_move(Ins,P),
    ejecutar_instruccion(Ri, Ins, Rf).

getAt([E|_], E, 0).

getAt([_|T], E, N) :-
    getAt(T, E, N1),
    N is N1 + 1.
   
move(L, P, X, Lf) :-
    move_(L, 0, P, X, Lf).

move_([_|T], P, P, X, [X|T]).

move_([H|T], I, P, X, [H|R]) :-
    In is I + 1,
    move_(T, In, P, X, R).

swap(Li, X, Y, Lf) :-
    nonvar(Li),
    getAt(Li, A, X),
    getAt(Li, B, Y),
    move(Li, X, B, Aux),
    move(Aux, Y, A, Lf),
    X < Y.

swap(Li, X, Y, Lf) :-
    nonvar(Lf),
    getAt(Lf, A, X),
    getAt(Lf, B, Y),
    move(Lf, X, B, Aux),
    move(Aux, Y, A, Li),
    X < Y.

% DEPTH-FIRST
% generador_de_codigo(Ri, Rf, L) :-
%     generador_de_codigo_(Ri, Rf, [Ri], L).

% generador_de_codigo_(Rf, Rf, _, []) :- !.

% generador_de_codigo_(Ri, Rf, _, [I]) :- 
%     eliminar_comodines(Rf, R2, _),
%     ejecutar_instruccion(Ri, I, R2),
%     !.

% generador_de_codigo_(Ri, Rf, V, [I|R]) :-
%     ejecutar_instruccion(Ri, I, Rip),
%     \+ member(Rip, V),
%     generador_de_codigo_(Rip, Rf, [Rip|V], R).

% BREATH-FIRST
generador_de_codigo(Ri, Rf, L) :- 
    % generador_de_codigo_(Rf, [n(Ri, [])], [], R),
    % !,
    % length(R, N),
    % length(R2, N),
    % generador_de_codigo_(Rf, [n(Ri, [])], [], L),
    % reverse(R2, L).
    generador_de_codigo_(Rf, [n(Ri, [])], [], R),
    reverse(R, L).

generador_de_codigo_(Rf, [n(Ra, I)|_], _, I) :- 
    eliminar_comodines(Rf, Ra, _).

generador_de_codigo_(Rf, [n(Ra, P1)|Ns], V, L) :-
    length(P1, N),
    findall(n(Rs, [I|P1]), 
        (ejecutar_instruccion(Ra, I, Rs),
            \+ (length(P2, N), member(n(Rs, P2), Ns)), 
            \+ member(Rs, V)), Es),
    append(Ns, Es, O),
    generador_de_codigo_(Rf, O, [Ra|V], L).


% sublist([], _).
% sublist([X|XS], [X|XSS]) :- sublist(XS, XSS).
% sublist([X|XS], [_|XSS]) :- sublist([X|XS], XSS).

members([], _).
members([H|T], L) :-
    members(T,L),
    member(H, L).

% in(El, [H|T]) :-
%     in_(T, El, H).

% in_(_, El, El).
% in_([H|T], El, _) :-
%     in_(T, El, H).

% length(Xs, N) :-
%     var(Xs),
%     integer(N),
%     length_num(N, Xs).

% length(Xs, N) :-
%     nonvar(Xs),
%     length_list(Xs, N).

% length_num(0, []).
% length_num(N, [_|Xs]) :-
%     N > 0,
%     N1 is N - 1,
%     length_num(N1, Xs).

% length_list([], 0).
% length_list([_|Xs], N) :-
%     length_list(Xs, N1),
%     N is N1 + 1.

% append([],L,L).
% append([H|T],L2,[H|L3]) :- append(T,L2,L3).

% reverse(Xs, Ys) :-
%     reverse_(Xs, [], Ys, Ys).

% reverse_([], Ys, Ys, []).
% reverse_([X|Xs], Rs, Ys, [_|Bound]) :-
%     reverse_(Xs, [X|Rs], Ys, Bound).

% ?- eliminar_comodines(regs(1,1,+,5,*), R, L).
% R = regs(1,1,+,5,_)L = [1, 1, +, 5]

% ?- ejecutar_instruccion(regs(1,2,+,5,*),swap(1,2),ES).
% ES = regs(2,1,+,5,*)

% ?- generador_de_codigo(regs(a,b,c),regs(a,a,*),L).
% L = [move(1)]

% ?- generador_de_codigo(regs(a,*,c),regs(c,a,*),L).
% L = [swap(1,2),swap(1,3)]

% ?- generador_de_codigo(regs(a,b,c,d),regs(a,d,a,b),L).
% L = [swap(1,2),swap(1,4),move(2),swap(1,2)]