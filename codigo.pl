:- module(_, _, [assertions]).
:- use_module(library(aggregates)).
:- use_module(library(lists)).

alumno_prode('Lopez', 'Rosende', 'Jorge', 'A180499').

:- doc(title, "Memoria y documentacion - Practica 2").

:- doc(author, "Jorge Lopez Rosende").

:- doc(module, "En esta practica se definen funciones para trabajar con registros e instruciones aplicadas a estos.

@section{Estructuras basicas}
Se define una estructura de datos @tt{regs/n} con n posiciones creando una lista de registros circular.

La instruccion @tt{move(i)}, que copia el registro de la posicion @tt{R(i)} en la @tt{R(i + 1)} para 0 < i <= n y @tt{R(1)} cuando @tt{i = n}.

La instruccion @tt{swap(i,j)}, que intercambia los valores de los registros en las posiciones indicadas,con @tt{i < j}.

@section{Funciones auxiliares}

Se ha creado una lista de funciones auxiliares para facilitar la realizacion de la practica.

@section{Funciones principales}

Se han desarrollado usando iso-prolog.

Tambien se han creado test que evaluan los predicados y las propiedades para evaluar detectar errores en la programacion.

@section{Tests automaticos}
Este modulo incluye aserciones que empiezan por @tt{% :- test}. Por ejemplo
@begin{verbatim}
:- test eliminar_comodines(R, Rs, L) : (R = regs(a,*), Rs = regs(a,_), L =[a]) + not_fails #``Caso base''.
@end{verbatim}

Estas definen casos de test. Dada una asercion @tt{% :- test Cabeza : Llamada =>
Salida + Comp}, @var{Cabeza} denota a que predicado se refiere la asercion,
@var{Llamada} describe los valores de entrada para el test, @var{Salida} define
los valores de salida @bf{si el predicado tiene exito} y @var{Comp} lo vamos a
usar para definir si el predicado tiene que tener exito para esa llamada o no:
@begin{itemize}
@item @tt{not_fails}: significa que la llamada al predicado con la entrada @var{Llamada} siempre tendra al menos una solucion.
@item @tt{fails}: significa que la llamada al predicado con la entrada @var{Llamada} siempre fallara.
@end{itemize}

@subsection{Lanzar los test automaticamente}
Para lanzar los test, selecciona en el menu de emacs @tt{CiaoDbg -> Run tests in current module}.

").

:- pred eliminar_comodines(R, Rs, L) #"Funcion principal que elimina los comodines de regs/n @var{R} y devuelve un regs/n en sin comodines en @var{Rs} crea una lista de simbolos y la devuelve en @var{L}. @includedef{eliminar_comodines/3}".
eliminar_comodines(R, Rs, L) :-
    check_regs(R, N, L2),
    replace(*, L2, L3),
    delete(L2, *, L),
    check_regs(Rs, N, L3).

:- test eliminar_comodines(R, Rs, L) : (R = regs(1,1,+,5,*)) => (Rs = regs(1,1,+,5,_), L = [1,1,+,5]) + not_fails #"Elimina los comodines de regs/n".
:- test eliminar_comodines(R, Rs, L) : (R = regs(1,1,*,5,*)) => (Rs = regs(1,1,_,5,_), L = [1,1,5]) + not_fails #"Elimina los multiples comodines de regs/n".
:- test eliminar_comodines(R, Rs, L) : (R = regs(1,1,*,5,*), Rs = regs(1,1,_,5,_),L = [1,1,p]) + fails #"Lista de elementos del registro erronea".
:- test eliminar_comodines(R, Rs, L) : (R = regs(*,*,*)) => (Rs = regs(_,_,_), L = []) + not_fails #"Elimina los multiples comodines de regs/n".

:- pred ejecutar_instruccion(Ri, I, Rf) #"Funcion principal que aplica una instruccion @var{I} al regs/n @var{Ri} y guarda su transformacion en un regs/n @var{Rf}. @includedef{ejecutar_instruccion/3}".
ejecutar_instruccion(Ri, move(I), Rf) :-
    check_regs(Ri, N, Li),
    nth(I, Li, E),
    P is I mod N,
    Pp is P + 1,
    move(Li, Pp, E, Lf),
    check_regs(Rf, N, Lf).

ejecutar_instruccion(Ri, swap(I, J), Rf) :-
    check_regs(Ri, N, Li),
    swap(Li, I, J, Lf),
    check_regs(Rf, N, Lf).

:- test ejecutar_instruccion(Ri, I, Rf) : (Ri = regs(1,2), I = swap(1,2)) => (Rf = regs(2,1)) + not_fails #"swap sobre regs/n".
:- test ejecutar_instruccion(Ri, I, Rf) : (Ri = regs(1,2), I = move(1)) => (Rf = regs(1,1)) + not_fails #"move sobre regs/n".
:- test ejecutar_instruccion(Ri, I, Rf) : (Ri = regs(1,2), I = move(2)) => (Rf = regs(2,2)) + not_fails #"move sobre regs/n".
:- test ejecutar_instruccion(Ri, I, Rf) : (Ri = regs(1,2), Rf = regs(2,1)) => (I = swap(1,2)) + not_fails #"obtener instruccion swap sobre regs/n".
:- test ejecutar_instruccion(Ri, I, Rf) : (Ri = regs(1,2), Rf = regs(1,1)) => (I = move(1)) + not_fails #"obtener instruccion move sobre regs/n".
:- test ejecutar_instruccion(Ri, I, Rf) : (Ri = regs(1,2), Rf = regs(2,2)) => (I = move(2)) + not_fails #"obtener instruccion move sobre regs/n".
:- test ejecutar_instruccion(Ri, I, Rf) : (Ri = regs(a,*,c), Rf = regs(c,a,*)) + fails #"obtener instruccion sobre regs/n que no es valida".

% DEPTH-FIRST
% generador_de_codigo(Ri, Rf, L) :-
%     generador_de_codigo_(Ri, Rf, [Ri], L).

% generador_de_codigo_(Rf, Rf, _, []) :- !.

% generador_de_codigo_(Ri, Rf, _, [I]) :- 
%     eliminar_comodines(Rf, R2, _),
%     ejecutar_instruccion(Ri, I, R2),
%     !.

% generador_de_codigo_(Ri, Rf, V, [I|R]) :-
%     ejecutar_instruccion(Ri, I, Rip),
%     \+ member(Rip, V),
%     generador_de_codigo_(Rip, Rf, [Rip|V], R).


:- pred generador_de_codigo(Ri, Rf, L) #"Funcion principal que genera una lista de movimientos minimos para transformar un regs/n @var{Ri} en el regs/n @var{Rf} y devuelve la lista de instrucciones en @var{L}.
Para llegar a la solucion se usa el algorintmo de recorrido de arbol BREATH-FIRST, permitiendo buscar todas las soluciones minimas alternativas. @includedef{generador_de_codigo/3}".
% BREATH-FIRST
generador_de_codigo(Ri, Rf, _) :- 
    eliminar_comodines(Ri, _, Li),
    eliminar_comodines(Rf, _, Lf),
    \+ members(Lf, Li),
    !, fail.

generador_de_codigo(Ri, Rf, L) :- 
    generador_de_codigo_(Rf, [n(Ri, [])], [], R),
    reverse(R, L).

:- pred generador_de_codigo_(Rf, La, Lc, L) #"Funcion principal que genera una lista de movimientos minimos para transformar un regs/n @var{Ri} en el regs/n @var{Rf} y devuelve la lista de instrucciones en @var{L}.
Para llegar a la solucion se usa el algorintmo de recorrido de arbol BREATH-FIRST, usa una lista abierta @var{La}, que guarda los nodos no visitados y una lista cerrada @var{Lc}, que guarda los nodos visitados, permitiendo buscar todas las soluciones minimas alternativas. @includedef{generador_de_codigo_/4}".

generador_de_codigo_(Rf, [n(Ra, I)|_], _, I) :- 
    eliminar_comodines(Rf, Ra, _).

generador_de_codigo_(Rf, [n(Ra, P1)|Ns], V, L) :-
    length(P1, N),
    findall(n(Rs, [I|P1]), 
        (
            ejecutar_instruccion(Ra, I, Rs),
            \+ (member(n(Rs, P2), Ns),length(P2, N)), 
            \+ member(Rs, V)
            ,Ra \= Rs
        ), 
        Es),
    filter_nodes(Es, Rf, T),
    append(Ns, T, O),
    generador_de_codigo_(Rf, O, [Ra|V], L).

:- test generador_de_codigo(Ri, Rf, L) : (Ri = regs(1,2), Rf = regs(2,2), L = [move(2)]) + not_fails #"obtener codigo regs/n".
:- test generador_de_codigo(Ri, Rf, L) : (Ri = regs(a,c,*), Rf = regs(c,a,*), L = [swap(1,2),swap(1,3)]) + not_fails #"obtener codigo regs/n".
:- test generador_de_codigo(Ri, Rf, L) : (Ri = regs(a,b,c,d), Rf = regs(a,d,a,b), L = [swap(1,2),swap(1,4),move(2),swap(1,2)]) + not_fails #"obtener codigo regs/n".

:- pred replace(E, L, Lf) #"Funcion auxiliar que remplaza todas las ocurrencias de un elemento @var{E} por una variable aleatoria @var{_} en una lista @var{L} y retorna una nueva lista en @var{Lf}. @includedef{replace/3}".
replace(_, [], []).
replace(O, [O|T], [_|T2]) :- replace(O, T, T2).
replace(O, [H|T], [H|T2]) :- H \= O, replace(O, T, T2).

:- pred check_regs(R, N, L) #"Funcion auxiliar que comprueba si un registro regs/n es valido y de vuelve el numero de registros totales en @var{N}, y devuelve los registros como una lista en @var{L}. @includedef{check_regs/3}".
check_regs(R, N, L) :-
    functor(R, regs, N),
    N > 1,
    R =.. T,
    arg(2,T,L).

:- pred move(L, I, E, Lf) #"Funcion auxiliar que remplaza el elemento en el indice @var{I} de la lista @var{L} por el elemento indicado en @var{E}, retorna una nueva lista en @var{Lf}. @includedef{move/4}".
move(L, P, X, Lf) :-
    move_(L, 1, P, X, Lf).

:- pred move_(L, I, P, E, Lf) #"Funcion auxiliar que remplaza el elemento en el indice @var{P} de la lista @var{L} empezando por el indice @var{I}, por el elemento indicado en @var{E}, retorna una nueva lista en @var{Lf}. @includedef{move_/5}".

move_([_|T], P, P, X, [X|T]).

move_([H|T], I, P, X, [H|R]) :-
    In is I + 1,
    move_(T, In, P, X, R).

:- pred swap(Li, X, Y, Lf) #"Funcion auxiliar que intercambia el elemento en la posicion @var{X} en la lista por otro elemento en la posicion @var{Y}, siempre que X < Y, retorna la nueva lista en @var{Lf}. @includedef{swap/4}".
swap(Li, X, Y, Lf) :-
    nonvar(Li),
    nth(X, Li, A),
    nth(Y, Li, B),
    move(Li, X, B, Aux),
    move(Aux, Y, A, Lf),
    X < Y.

:- pred filter_nodes(Li, Rf, Lf) #"Funcion auxiliar que filtra los nodos generados dejando solo los que permitan alcanzar el nodo final. @includedef{filter_nodes/3}".
filter_nodes([], _, []).

filter_nodes([n(Ra, I)|ES], Rf, [n(Ra, I)|L]) :- 
    eliminar_comodines(Ra, _, La),
    eliminar_comodines(Rf, _, Lf),
    members(Lf, La),
    filter_nodes(ES, Rf, L).

filter_nodes([n(Ra, _)|ES], Rf, L) :- 
    eliminar_comodines(Ra, _, La),
    eliminar_comodines(Rf, _, Lf),
    \+ members(Lf, La),
    filter_nodes(ES, Rf, L).

:- pred members(Li, Lf) #"Funcion auxiliar que compruba si todos los elementos de la @var{Li} estan en la lista @var{Lf} sin importar su orden o su repeticion. @includedef{members/2}".
members([], _).
members([H|T], L) :-
    members(T,L),
    member(H, L).

% ?- eliminar_comodines(regs(1,1,+,5,*), R, L).
% R = regs(1,1,+,5,_)L = [1, 1, +, 5]

% ?- ejecutar_instruccion(regs(1,2,+,5,*),swap(1,2),ES).
% ES = regs(2,1,+,5,*)

% ?- generador_de_codigo(regs(a,b,c),regs(a,a,*),L).
% L = [move(1)]

% ?- generador_de_codigo(regs(a,*,c),regs(c,a,*),L).
% L = [swap(1,2),swap(1,3)]

% ?- generador_de_codigo(regs(a,b,c,d),regs(a,d,a,b),L).
% L = [swap(1,2),swap(1,4),move(2),swap(1,2)]

% findall(L, generador_de_codigo(regs(a,*,c), regs(c,a,*), L), List),length(List,N).
% List = [
    % [move(1),move(3)],
    % [move(1),swap(1,3)],
    % [swap(1,2),move(3)],
    % [swap(1,2),swap(1,3)],
    % [swap(1,3),swap(2,3)],
    % [swap(2,3),swap(1,2)],
    % [move(1),swap(1,3),move(2)],
    % [move(1),swap(1,3),swap(2,3)]
% ],
% N = 8 ? 
% no